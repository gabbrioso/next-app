import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
    return(
        <Row>
            <Col>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed qui nihil illum quibusdam voluptatem impedit, ullam veritatis saepe reprehenderit nesciunt aut praesentium tenetur omnis libero voluptates sequi consectetur iure nostrum!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed qui nihil illum quibusdam voluptatem impedit, ullam veritatis saepe reprehenderit nesciunt aut praesentium tenetur omnis libero voluptates sequi consectetur iure nostrum!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be Part of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed qui nihil illum quibusdam voluptatem impedit, ullam veritatis saepe reprehenderit nesciunt aut praesentium tenetur omnis libero voluptates sequi consectetur iure nostrum!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

        </Row>
    )
}