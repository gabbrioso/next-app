export default [
    {
        email: "admin@mail.com",
        password: "admin123",
        isAdmin: true
    },
    {
        email: "rem@mail.com",
        password: "rem123",
        isAdmin: false
    }
]